# Assignment 5 Observable Chart

Refactor the current repository such that an Observable pattern is used to update the chart when data files are updated.
Provide the names of each entity in the Observer pattern (e.g. Subject, Observer) and their corresponding implementation.